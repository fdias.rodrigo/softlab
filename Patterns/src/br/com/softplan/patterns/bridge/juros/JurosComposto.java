package br.com.softplan.patterns.bridge.juros;

public class JurosComposto implements IJuros {

	@Override
	public void aplica(double percent) {
		System.out.println(String.format("aplicou juros composto de %s porcento", percent));
	}

}
