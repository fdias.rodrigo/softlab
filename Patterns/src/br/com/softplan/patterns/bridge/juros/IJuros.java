package br.com.softplan.patterns.bridge.juros;

public interface IJuros {

	void aplica(double percent);
}
