package br.com.softplan.patterns.bridge.juros;

public class JurosSimples implements IJuros{

	@Override
	public void aplica(double percent) {
		System.out.println(String.format("aplicou juros simples de %s porcento", percent));	
	}

}
