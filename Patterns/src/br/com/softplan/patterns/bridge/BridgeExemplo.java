package br.com.softplan.patterns.bridge;

import br.com.softplan.patterns.bridge.juros.JurosComposto;
import br.com.softplan.patterns.bridge.juros.JurosSimples;
import br.com.softplan.patterns.dto.ParcelaCPG;
import br.com.softplan.patterns.dto.ParcelaCRC;

public class BridgeExemplo {
	
	public static void main(String[] args) {
		
		ParcelaCPG parcelaCpg1 = new ParcelaCPG(new JurosComposto());
		ParcelaCPG parcelaCpg2 = new ParcelaCPG(new JurosSimples());
		ParcelaCRC parcelaCrc1 = new ParcelaCRC(new JurosComposto());
		ParcelaCRC parcelaCrc2 = new ParcelaCRC(new JurosSimples());
		
		parcelaCpg1.aplicaJuros(20d);
		parcelaCpg2.aplicaJuros(20d);
		parcelaCrc1.aplicaJuros(20d);
		parcelaCrc2.aplicaJuros(20d);
	}

}
