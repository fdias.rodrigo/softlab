package br.com.softplan.patterns.dto;

import br.com.softplan.patterns.bridge.juros.IJuros;

public class ParcelaCPG extends Parcela {

	private Double valorPago = Double.valueOf(0);
	
	public ParcelaCPG(IJuros juros) {
		super(juros);
	}

	@Override
	public Double getValorTotal() {
		return valorPago + getValorAcrescimos() ;
	}
	
	public Double getValorPago() {
		return valorPago;
	}

	public void setValorPago(Double valorPago) {
		this.valorPago = valorPago;
	}
}
