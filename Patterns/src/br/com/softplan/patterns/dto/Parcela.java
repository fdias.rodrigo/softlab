package br.com.softplan.patterns.dto;

import br.com.softplan.patterns.bridge.juros.IJuros;
import br.com.softplan.patterns.composite.ICalculavel;

public abstract class Parcela implements ICalculavel {

	protected IJuros juros;
	protected Double valorAcrescimos;
	
	protected Parcela(IJuros juros) {
		this();
		this.juros = juros;
	}
	
	protected Parcela() {
		valorAcrescimos = Double.valueOf(0);
	}
	
	public Double getValorAcrescimos() {
		return valorAcrescimos;
	}

	public void setValorAcrescimos(Double valorAcrescimos) {
		this.valorAcrescimos = valorAcrescimos;
	}

	public void aplicaJuros(double percent) {
		juros.aplica(percent);
	}
}
