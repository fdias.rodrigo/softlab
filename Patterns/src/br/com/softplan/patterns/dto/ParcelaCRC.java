package br.com.softplan.patterns.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.softplan.patterns.bridge.juros.IJuros;

public class ParcelaCRC extends Parcela {
	
	private List<BaixaCRC> baixas;
	private Double taxaAdministrativa;
	
	public ParcelaCRC(IJuros juros) {
		super(juros);
		baixas = new ArrayList<>();
		taxaAdministrativa = Double.valueOf(0);
	}
	
	@Override
	public void aplicaJuros(double percent) {
		juros.aplica(percent);
		System.out.println(" na parcela do CRC");
	}

	@Override
	public Double getValorTotal() {
		return baixas.stream().mapToDouble(BaixaCRC::getValorTotal).sum() 
				+ getTaxaAdministrativa() 
				+ getValorAcrescimos();
	}

	public List<BaixaCRC> getBaixas() {
		return baixas;
	}

	public Double getTaxaAdministrativa() {
		return taxaAdministrativa;
	}

	public void setTaxaAdministrativa(Double taxaAdministrativa) {
		this.taxaAdministrativa = taxaAdministrativa;
	}
}
