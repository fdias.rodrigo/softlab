package br.com.softplan.patterns.dto;

import br.com.softplan.patterns.composite.ICalculavel;

public class BaixaCRC implements ICalculavel{

	private Double valorRecebido;
	
	@Override
	public Double getValorTotal() {
		return getValorRecebido();
	}

	public Double getValorRecebido() {
		return valorRecebido;
	}

	public void setValorRecebido(Double valorRecebido) {
		this.valorRecebido = valorRecebido;
	}

}
