package br.com.softplan.patterns.composite;

import java.util.ArrayList;
import java.util.List;

import br.com.softplan.patterns.bridge.juros.IJuros;
import br.com.softplan.patterns.bridge.juros.JurosComposto;
import br.com.softplan.patterns.dto.BaixaCRC;
import br.com.softplan.patterns.dto.ParcelaCRC;

public class CompositeExemplo {
	
	List<ICalculavel> calculaveis = new ArrayList<>();

	public static void main(String[] args) {
		CompositeExemplo composite = new CompositeExemplo();
		composite.run();
	}
	
	public void run() {
		ParcelaCRC parcela1 = addParcelaCRC(new JurosComposto(), 0);
		parcela1.getBaixas().add(addBaixa(23.5));
		parcela1.getBaixas().add(addBaixa(15.0));
		
		ParcelaCRC parcela2 = addParcelaCRC(new JurosComposto(), 8);
		parcela2.getBaixas().add(addBaixa(66.5));
		
		calculaveis.add(parcela1);
		calculaveis.add(parcela2);
		calculaveis.add(addBaixa(33.0));
		
		double valorTotal = calculaveis.stream().mapToDouble(ICalculavel::getValorTotal).sum();
		
		System.out.println("Valor Total: " + valorTotal);
	}
	
	private BaixaCRC addBaixa(double valorRecebido) {
		BaixaCRC baixa = new BaixaCRC();
		baixa.setValorRecebido(valorRecebido);
		return baixa;
	}
	
	private ParcelaCRC addParcelaCRC(IJuros juros, double vlTaxaAdm) {
		ParcelaCRC parcelaCRC = new ParcelaCRC(juros);
		parcelaCRC.setTaxaAdministrativa(vlTaxaAdm);
		return parcelaCRC;
	}
	
	
}
