package br.com.softplan.patterns.composite;

public interface ICalculavel {

	Double getValorTotal();
}
