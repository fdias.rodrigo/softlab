package br.com.softplan.qualitycode.predicate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import br.com.softplan.qualitycode.filters.ContainsTextFileFilter;
import br.com.softplan.qualitycode.helper.FileHelper;

public class NoneUsedClassPredicate implements Predicate<File>{

	@Override
	public boolean test(File arquivo) {
		FileHelper fileHelper = new FileHelper();
		List<File> fileList = new ArrayList<>();
		String pathClass = arquivo.getPath().replaceAll("\\\\", ".");
		ContainsTextFileFilter containTextFileFilter = new ContainsTextFileFilter(pathClass);
		fileHelper.procuraFilesRecursivamente(fileHelper.getRaizDirectory(), containTextFileFilter, fileList);
		
		return fileList.isEmpty();
	}
	
}
