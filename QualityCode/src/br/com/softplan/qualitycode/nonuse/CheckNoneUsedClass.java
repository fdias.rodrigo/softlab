package br.com.softplan.qualitycode.nonuse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.softplan.qualitycode.filters.JavaFileFilter;
import br.com.softplan.qualitycode.helper.FileHelper;
import br.com.softplan.qualitycode.predicate.NoneUsedClassPredicate;

public class CheckNoneUsedClass {

	public static void main(String[] args) {
		CheckNoneUsedClass check = new CheckNoneUsedClass();
		check.testClasseNaoUsada();
	}
	
	public void testClasseNaoUsada() {
		List<File> fileList = new ArrayList<>();
		FileHelper fileHelper = new FileHelper();
		File fileSrc = fileHelper.getRaizDirectory();
		fileHelper.procuraFilesRecursivamente(fileSrc, new JavaFileFilter(), fileList);
		
		fileList.stream().filter(file -> new NoneUsedClassPredicate().test(file)).toList();

		System.out.println(fileList);
	}
	

	
}
