package br.com.softplan.qualitycode.filters;

import java.io.File;
import java.io.FileFilter;

import br.com.softplan.qualitycode.helper.FileHelper;

public class ContainsTextFileFilter  implements FileFilter{

	private String textFilter;
	
	public ContainsTextFileFilter(String textFileFilter) {
		this.textFilter = textFileFilter;
	}
	
	/**
	 * FileTreatmentException é usado no FileHelper
	 * Prever uso de classe no mesmo pacote. Aí não compara o pacote
	 */
	@Override
	public boolean accept(File file) {
		String contentFile = new FileHelper().readFileAsString(file.getAbsolutePath());
		return contentFile.contains(textFilter);
	}
}
