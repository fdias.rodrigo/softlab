package br.com.softplan.qualitycode.filters;

import java.io.File;
import java.io.FileFilter;

public class JavaFileFilter implements FileFilter{

	@Override
	public boolean accept(File file) {
		return file.getAbsolutePath().endsWith(".java");
	}


}
