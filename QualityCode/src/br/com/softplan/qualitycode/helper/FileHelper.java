package br.com.softplan.qualitycode.helper;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import br.com.softplan.qualitycode.exception.FileTreatmentException;

public class FileHelper {
	
	private static final String RAIZ = "src";
	
	public File getRaizDirectory() {
		return Paths.get(RAIZ).toFile();
	}

    public void procuraFilesRecursivamente(File dir, FileFilter filter, List<File> fileList) {
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                procuraFilesRecursivamente(file, filter, fileList);
            } else {
                if (filter.accept(file)) {
                	 fileList.add(file);
                }
            }
        }
    }
    
	public String readFileAsString(String fileName) {
	   try {
		return new String(Files.readAllBytes(Paths.get(fileName)));
	} catch (IOException e) {
		throw new FileTreatmentException("Erro ao ler conteúdo do arquivo", e);
	}
	}
}
