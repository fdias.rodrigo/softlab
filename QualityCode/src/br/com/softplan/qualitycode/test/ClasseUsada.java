package br.com.softplan.qualitycode.test;

public class ClasseUsada {
	
	public static void main(String[] args) {
		ClasseUsada used = new ClasseUsada();
		used.metodoUsado();
	}

	public void metodoNaoUsado() {
		System.err.println("método não usado");
	}
	
	public void metodoUsado() {
		System.out.println("método usado");
	}
}
