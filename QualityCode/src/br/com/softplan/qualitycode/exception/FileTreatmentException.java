package br.com.softplan.qualitycode.exception;

import java.io.IOException;

public class FileTreatmentException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public FileTreatmentException(String mensagem, IOException e) {
		super(mensagem, e);
	}

}
