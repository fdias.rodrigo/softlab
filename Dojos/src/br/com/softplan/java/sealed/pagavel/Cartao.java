package br.com.softplan.java.sealed.pagavel;

import br.com.softplan.java.sealed.Pagavel;

public sealed class Cartao implements Pagavel {

	final class CartaoDebito extends Cartao{}
	
	final class CartaoCredito extends Cartao{}
}
