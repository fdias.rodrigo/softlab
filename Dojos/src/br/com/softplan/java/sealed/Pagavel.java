package br.com.softplan.java.sealed;

import br.com.softplan.java.sealed.pagavel.Boleto;
import br.com.softplan.java.sealed.pagavel.Cartao;
import br.com.softplan.java.sealed.pagavel.Dinheiro;
import br.com.softplan.java.sealed.pagavel.Pix;

public sealed interface Pagavel permits Cartao, Boleto, Pix, Dinheiro {

}
