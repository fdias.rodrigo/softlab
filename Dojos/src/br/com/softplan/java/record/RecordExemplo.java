package br.com.softplan.java.record;

public class RecordExemplo {

	public static void main(String[] args) {
		RecordExemplo exemplo = new RecordExemplo();
		exemplo.executa();
	}
	
	public void executa() {
		UsuarioRecord recordOne = new UsuarioRecord("Rodrigo", "pass123@");
		UsuarioRecord recordDuplicated = new UsuarioRecord("Rodrigo", "pass123@");
		System.out.println("Mesmos valores de atributos? " + recordOne.equals(recordDuplicated));
	}
	
}
